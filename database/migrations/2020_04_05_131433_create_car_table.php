<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car', function (Blueprint $table) {
            $table->Increments("id");
            $table->string("color");
            $table->integer("date");
            $table->integer("mileage");
            $table->integer("price");
            $table->integer("users_id")->unsigned();
            $table->integer("model_id")->unsigned();
            $table->integer("brand_id")->unsigned();
            $table->foreign("brand_id")->on("brand")->references("id")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("model_id")->on("model")->references("id")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("users_id")->on("users")->references("id")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car');
    }
}
