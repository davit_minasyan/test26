<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model', function (Blueprint $table) {
            $table->Increments("id");
            $table->string("model");
            $table->integer("brand_id")->unsigned();
            $table->foreign("brand_id")->on("brand")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();
        });

}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model');
    }
}
