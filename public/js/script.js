$(".select").click(function() {
    var x = $(".select").val()
    $.ajax({
        type: "get",
        url: "/Model/"+x,
        data: {"_token": $("#token").val()},
        success: function (r) {
            console.log(r)
            if(r === "exit") {
                alert("Вы сможете добавить автомобиль только после входа в систему")
            }else{
                $(".select_model").empty()
                r.forEach(function (i) {
                $(".select_model").append(`<option value="${i.id}">${i.model}</option>`)
                })
            }
        }
    })

})

$(".button_create_ajax").click(function() {
    event.preventDefault();
    var _token = $("#token").val()
    var form = $('.car_ajax')[0];
    var data = new FormData(form);
    var x = $(this).parent().children(".select").val()
    var y = $(this).parent().children(".select_model").val()
    data.append("_token", _token);
    data.append("model_id", y);
    data.append("brand_id", x);
    var w = $(this).parent().children(".color").val()
    var p = $(this).parent().children(".date").val()
    var q = $(this).parent().children(".mileage").val()
    var e = $(this).parent().children(".price").val()
    if(isNaN(p) || isNaN(q) || isNaN(e) || y === null || y ==="Model" || x ==="Brand" || w === "" || q ==="" || p === "" || e === "" || w.length >=20 || q.length >=11 || p.length >= 11 || e.length >= 11){

        if(x ==="Brand"){
            $(".errors_update_brand_empty").show(500)
        }else{
            $(".errors_update_brand_empty").hide(50)
        }

        if(y===null || y==="Model"){
            $(".errors_update_model_empty").show(500)
        }else{
            $(".errors_update_model_empty").hide(50)
        }

        if(w ===""){
            $(".errors_update_color_30").hide(50)
            $(".errors_update_color_empty").show(500)
        }else if(w.length >=20){
            $(".errors_update_color_empty").hide(50)
            $(".errors_update_color_30").show(500)
        }else{
            $(".errors_update_color_empty").hide(50)
            $(".errors_update_color_30").hide(50)
        }

        if(isNaN(p)){
            $(".errors_update_date_empty").hide()
            $(".errors_update_date_10").hide()
            $(".errors_update_date_number").show(500)
        }else if(p ===""){
            $(".errors_update_date_number").hide()
            $(".errors_update_date_10").hide()
            $(".errors_update_date_empty").show(500)
        }else if(p.length >=11){
            $(".errors_update_date_empty").hide()
            $(".errors_update_date_number").hide()
            $(".errors_update_date_10").show(500)
        }else{
            $(".errors_update_date_empty").hide()
            $(".errors_update_date_10").hide()
            $(".errors_update_date_number").hide()
        }

        if(isNaN(q)){
            $(".errors_update_mieage_empty").hide()
            $(".errors_update_mileage_10").hide()
            $(".errors_update_mileage_number").show(500)
        }else if(q ===""){
            $(".errors_update_mileage_number").hide()
            $(".errors_update_mileage_10").hide()
            $(".errors_update_mileage_empty").show(500)
        }else if(q.length >=11){
            $(".errors_update_mileage_empty").hide()
            $(".errors_update_mileage_number").hide()
            $(".errors_update_mileage_10").show(500)
        }else{
            $(".errors_update_mileage_empty").hide()
            $(".errors_update_mileage_10").hide()
            $(".errors_update_mileage_number").hide()
        }

        if(isNaN(e)){
            $(".errors_update_price_empty").hide()
            $(".errors_update_price_10").hide()
            $(".errors_update_price_number").show(500)
        }else if(e ===""){
            $(".errors_update_price_number").hide()
            $(".errors_update_price_10").hide()
            $(".errors_update_price_empty").show(500)
        }else if(e.length >=11){
            $(".errors_update_price_empty").hide()
            $(".errors_update_price_number").hide()
            $(".errors_update_price_10").show(500)
        }else{
            $(".errors_update_price_empty").hide()
            $(".errors_update_price_10").hide()
            $(".errors_update_price_number").hide()
        }

    }else {
        $(".errors_update_brand_empty").hide(50)
        $(".errors_update_color_empty").hide(50)
        $(".errors_update_color_30").hide(50)
        $(".errors_update_date_empty").hide(50)
        $(".errors_update_date_10").hide(50)
        $(".errors_update_date_number").hide(50)
        $(".errors_update_price_empty").hide(50)
        $(".errors_update_price_10").hide(50)
        $(".errors_update_price_number").hide(50)
        $(".errors_update_mileage_empty").hide(50)
        $(".errors_update_mileage_10").hide(50)
        $(".errors_update_mileage_number").hide(50)

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/Car",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                console.log(data)
            }
        })
        alert("Вы успешно добавили свой автомобиль")
    }
})

$(".remove_image").click(function(){
    var x = $(this).attr("data-id")
    $(this).parent().remove()
    $.ajax({
        url:"/Car/"+x,
        type:"DELETE",
        data:{"_token": $("#token").val()},
        success:function(r){
            console.log(r)
        }
    })
})

$(".model_div").click(function(){
    var x = $(this).attr("data-id")
    $(this).remove()
    $.ajax({
        url:"/Model/"+x,
        type:"DELETE",
        data:{"_token": $("#token").val()},
        success:function(r){
            console.log(r)
        }
    })
})

$("ul").click(function(){
    $(this).css({"color":"red"})
})

$(".send_update").click(function(){
    event.preventDefault();
    var x = $(this).attr("data-id")
    var w = $(this).parent().children(".color").val()
    var y = $(this).parent().children(".date").val()
    var z = $(this).parent().children(".mileage").val()
    var p = $(this).parent().children(".price").val()
    console.log(w+y+z+p)
    if(isNaN(y) || isNaN(z) || isNaN(p) || w === "" || y ==="" || p === "" || z === "" || w.length >=20 || y.length >=11 || z.length >= 11 || p.length >= 10){

        if(w ===""){
            $(".errors_update_color_30").hide(50)
            $(".errors_update_color_empty").show(500)
        }else if(w.length >=20){
            $(".errors_update_color_empty").hide(50)
            $(".errors_update_color_30").show(500)
        }else{
            $(".errors_update_color_empty").hide(50)
            $(".errors_update_color_30").hide(50)
        }

        if(isNaN(y)){
            $(".errors_update_date_empty").hide()
            $(".errors_update_date_10").hide()
            $(".errors_update_date_number").show(500)
        }else if(y ===""){
            $(".errors_update_date_number").hide()
            $(".errors_update_date_10").hide()
            $(".errors_update_date_empty").show(500)
        }else if(y.length >=11){
            $(".errors_update_date_empty").hide()
            $(".errors_update_date_number").hide()
            $(".errors_update_date_10").show(500)
        }else{
            $(".errors_update_date_empty").hide()
            $(".errors_update_date_10").hide()
            $(".errors_update_date_number").hide()
        }

        if(isNaN(z)){
            $(".errors_update_mieage_empty").hide()
            $(".errors_update_mileage_10").hide()
            $(".errors_update_mileage_number").show(500)
        }else if(z ===""){
            $(".errors_update_mileage_number").hide()
            $(".errors_update_mileage_10").hide()
            $(".errors_update_mileage_empty").show(500)
        }else if(z.length >=11){
            $(".errors_update_mileage_empty").hide()
            $(".errors_update_mileage_number").hide()
            $(".errors_update_mileage_10").show(500)
        }else{
            $(".errors_update_mileage_empty").hide()
            $(".errors_update_mileage_10").hide()
            $(".errors_update_mileage_number").hide()
        }

        if(isNaN(p)){
            $(".errors_update_price_empty").hide()
            $(".errors_update_price_10").hide()
            $(".errors_update_price_number").show(500)
        }else if(p ===""){
            $(".errors_update_price_number").hide()
            $(".errors_update_price_10").hide()
            $(".errors_update_price_empty").show(500)
        }else if(p.length >=10){
            $(".errors_update_price_empty").hide()
            $(".errors_update_price_number").hide()
            $(".errors_update_price_10").show(500)
        }else{
            $(".errors_update_price_empty").hide()
            $(".errors_update_price_10").hide()
            $(".errors_update_price_number").hide()
        }

    }else{
        $(".errors_update_color_empty").hide(50)
        $(".errors_update_color_30").hide(50)
        $(".errors_update_date_empty").hide(50)
        $(".errors_update_date_10").hide(50)
        $(".errors_update_date_number").hide(50)
        $(".errors_update_price_empty").hide(50)
        $(".errors_update_price_10").hide(50)
        $(".errors_update_price_number").hide(50)
        $(".errors_update_mileage_empty").hide(50)
        $(".errors_update_mileage_10").hide(50)
        $(".errors_update_mileage_number").hide(50)
        $(".color_name_edit").html(w)
        $(".date_name_edit").html(y)
        $(".mileage_name_edit").html(z)
        $(".price_name_edit").html(p)
        $.ajax({
            url:"/Car/"+x,
            type:"PUT",
            data:{"_token": $("#token").val(),color:w,date:y,mileage:z,price:p},
            success:function(r){
                console.log(r)
            }
        })
    }
})

$(".select").click(function(){
    $(this).css({"outline":"none"})
})

$(".select_model").click(function(){
    $(this).css({"outline":"none"})
})

$(".delete_brand_x").click(function(){
    var x = $(this).attr("data-id")
    $(this).parent().remove()
    $.ajax({
        url:"/Brand/"+x,
        type:"DELETE",
        data:{"_token": $("#token").val()},
        success:function(r){
            console.log(r)
        }
    })
})

$(".delete_user").click(function(){
    var x = $(this).attr("data-id")
    $(this).parent().remove()
    $.ajax({
        url:"/User/"+x,
        type:"Delete",
        data:{"_token": $("#token").val()},
        success:function(r){
            console.log(r)
        }
    })
})

$(".brand_search").click(function(){
    var x = $(this).val()
    if(x !== "Brand"){
        $.ajax({
            url:"/Brand_search/"+x,
            type:"get",
            data:{"_token": $("#token").val()},
            success:function(r){
                console.log(r)
                $(".all").empty();
                r.forEach(function(i){
                    var el = "/img/avatar_car.png"
                    if (i.image.length) {
                        console.log("Has images");
                        el = '/storage/'+i.image[0].image
                    }
                    $(".all").append(`
                    <div class="car">
                        <a class="href_show"  data-id="${i.id}">
                            <span><img src="${el}" width="100px" height="100px"></span>
                            <span>Марка <span class="brand_car_name">${i.brand[0].brand}</span></span>
                            <span>Модель <span class="model_car_name">${i.model[0].model}</span></span>
                            <span>Дата <span class="date_car_name">${i.date}</span></span>
                            <span>Цена <span class="price_car_name">${i.price}</span></span>
                        </a>
                     </div>`)
                })
            }
        })
    }else{
        $.ajax({
            url:"/brand_all",
            type:"get",
            data:{"_token": $("#token").val()},
            success:function(r){
                console.log(r)
                $(".all").empty();
                r.forEach(function(i){
                    var el = "/img/avatar_car.png"
                    if (i.image.length) {
                        el = '/storage/'+i.image[0].image
                    }
                    var all = $(`
                    <div class="car">
                        <a class="href_show" data-id="${i.id}">
                            <span><img src="${el}" width="100px" height="100px"></span>
                            <span>Марка <span class="brand_car_name">${i.brand[0].brand}</span></span>
                            <span>Модель <span class="model_car_name">${i.model[0].model}</span></span>
                            <span>Дата <span class="date_car_name">${i.date}</span></span>
                            <span>Цена <span class="price_car_name">${i.price}</span></span>
                        </a>
                    </div>`)
                    $(".all").append(all)
                })
            }
        })
    }
})

$(document).on("click",".href_show",function(){
    var x = $(this).attr("data-id")
    window.location.assign("/Car/"+x)
})

$(".image").click(function(){
    $(".image").css({"border":"0px solid"})
    var x = $(this).attr("src")
    $(this).css({"border":"2px solid"})
    $(".img_show").remove()
    $(this).parent().parent().children(".img_profile_car").append(`<img src="`+x+`" class='img_show'>`)

})