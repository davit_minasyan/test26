<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view("login","login_registration/login");
Route::view("registration","login_registration/registration");
Route::post("Confirm/enter","LoginController@enter");
Route::post("Confirm/enter","LoginController@enter");
Route::get("car/index","CarController@index");
Route::get("Car/create","CarController@create");
Route::get("Car/{id}","CarController@show");
Route::get("/brand_all","BrandController@brand_all");
Route::get("Brand_search/{x}","BrandController@search");
Route::resource("login_registration","LoginController");
Route::resource("Model","ModelController");
Route::middleware("Auth_Middleware")->group(function() {
    Route::get('user/dashboard','UserController@dashboard');
    Route::get("user/logout","UserController@logout");
    Route::resource("User", "UserController");
    Route::resource("Brand","BrandController");
    Route::resource("Car", "CarController");
});
