<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table = "model";
    public $timestamps = false;
    protected $fillable = [
        "model",
        "brand_id",
    ];

    public function brand(){
        return $this->belongsTo("App\brand","brand_id");
    }

}
