<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "image";
    public $timestamps = false;
    protected $fillable = [
        "image",
        "car_id",
    ];

    static public function store($x){
        foreach($x["image"] as $image) {
            $z["image"] = $image->store("uploads", "public");
            $z["car_id"] = $x["car_id"];
            Image::create($z);
        }
    }
}
