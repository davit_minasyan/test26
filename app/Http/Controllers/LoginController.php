<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginValidate;
use App\Http\Requests\RegistrationValidate;
use Illuminate\Support\Facades\Hash;
use App\Users;


class LoginController extends Controller
{

    public function store(RegistrationValidate $x){
        $x["password"] = Hash::make($x['password']);
        Users::create($x->all());

        return Redirect::to("/login");
    }

    public function enter(LoginValidate $x)
    {
        unset($x["_token"]);
        if (empty(Auth::attempt($x->all()))) {
            return Redirect::to("/login")->withErrors(["email"=>"Incorrect login or password "]);
        }
        return Redirect::to("/user/dashboard");
    }
}
