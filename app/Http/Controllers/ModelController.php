<?php

namespace App\Http\Controllers;

use App\Models;

class ModelController extends Controller
{

    public function show($id){
        if(empty(auth()->id())){
            return "exit";
        }else{
            $model = Models::where("brand_id",$id)->get();
            return $model;
        }
    }

    public function destroy($id){
        Models::where("id",$id)->delete();
    }

}
