<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Image;
use App\Car;
use App\Models;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    public function index(){
        $brand = Brand::all();
        return view("user/brand",compact("brand"));
    }

    public function show($id){
           Brand::where("id",$id)->delete();
           return Redirect::to("/Brand");
    }

    public function search($id){
        $car = Car::where(["brand_id"=>$id])->get();
        $car = $car->map(function ($car) {
        $image = Image::where('car_id',$car->id)->get();
        $brand = brand::where("id",$car->brand_id)->get();
        $model = Models::where('id',$car->model_id)->get();
        return [
                'id' => $car->id,
                'color' => $car->color,
                'date' => $car->date,
                'mileage' => $car->mileage,
                'price' => $car->price,
                'image' => $image,
                'brand' => $brand,
                'model' => $model,
            ];
        });
        return $car;
    }

    public function brand_all(){
        $car = Car::all();
        $car = $car->map(function ($car) {
            $image = Image::where('car_id',$car->id)->get();
            $brand = brand::where("id",$car->brand_id)->get();
            $model = Models::where('id',$car->model_id)->get();
            return [
                'id' => $car->id,
                'color' => $car->color,
                'date' => $car->date,
                'mileage' => $car->mileage,
                'price' => $car->price,
                'image' => $image,
                'brand' => $brand,
                'model' => $model,
            ];
        });
        return $car;
    }

    public function destroy($id){
        Brand::where("id",$id)->delete();
    }
}
