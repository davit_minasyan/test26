<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Brand;
use App\Image;
use App\Models;

class CarController extends Controller
{
    public function index(){
        $cars = Car::all();
        $brand = Brand::all();
        return view("car/index",compact("cars","brand"));
    }

    public function model(Request $x){
        $model = Models::where(["brand_id" => $x["brand_id"]])->get();
        return $model;
    }

    public function create(){
        $brand = Brand::all();
        $model = Models::all();
        return view("car/create",compact("brand","model"));
    }

    public function store(Request $x){
            unset($x["_token"]);
            $x["users_id"] = auth()->id();
            Car::create($x->all());
            $car = Car::where(["users_id"=>$x["users_id"]])->get()->last();
            $x["car_id"] = $car["id"];
            print_r($x["car_id"]);
            if(!empty($x["image"])){
                Image::store($x->all());
            }
    }

    public function show($id){
        $car = Car::where("id",$id)->get();
        return view("car/show",compact("car"));
    }

    public function edit($id){
        $car = Car::where("id",$id)->get();
        return view("car/edit",compact("car"));
    }

    public function update(Request $x,$id){
        $x["users_id"] = auth()->id();
        unset($x["brand"],$x["_token"]);
        Car::where("id",$id)->update($x->all());
    }

    public function destroy($id){
        image::where("id",$id)->delete();
    }

}
