@extends("layouts/layouts")
@section("layouts")
    <div class="avatar_user">
        <img src="{{URL::to('/img/avatar_user.png')}}">
        <h1 class="h1_avatar_user">
            {{Auth::user()->name}}
            {{Auth::user()->surname}}
            {{Auth::user()->age}}
        </h1>
    </div>
@endsection