@extends("layouts\layouts")
@section("layouts")
    <div class="all_brand">
        @foreach($brand as $brand)
            <div class="delete_brand">
                <button class="delete_brand_x" data-id="{{$brand->id}}">x</button>
                <div class="div_brand">{{$brand->brand}}</div>
                @foreach($brand->model as $brand)
                    <div class="model_div" data-id="{{$brand->id}}">
                        <div class="model">{{$brand->model}}</div>
                        <div class="delete_model">X</div>
                    </div>
                @endforeach
            </div>
            <div class="area"></div>
        @endforeach
    </div>
@endsection