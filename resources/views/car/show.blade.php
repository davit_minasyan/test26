@extends("layouts/layouts")
@section("layouts")
    <div class="car1">
        <div class="area_show"></div>
        @if(count($car[0]->image)>0)
            <span class="img_profile_car"><img src="{{URL::to('/storage/'.$car[0]->image[0]->image)}}" class="img_show"></span>
        @else
            <span class="img_profile_car"><img src="{{URL::to('/img/avatar_car.png')}}"></span>
        @endif
        <div class="car_image">
            @foreach($car[0]->image as $image)
                <img src="{{URL::to('/storage/'.$image->image)}}" class="image" width="50px" height="50px">
            @endforeach
        </div>
            <span>Марка `<span>{{$car[0]->model->brand->brand}}</span></span>
            <span>Модель `<span>{{$car[0]->model->model}}</span></span>
            <span>Цвет ` <span>{{$car[0]->color}}</span></span>
            <span>Дата `<span>{{$car[0]->date}}</span></span>
            <span>Пробег ` <span>{{$car[0]->mileage}}</span></span>
            <span>Цена ` <span>{{$car[0]->price}}</span></span>
    </div>
@endsection