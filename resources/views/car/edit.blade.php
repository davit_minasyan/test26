@extends("layouts/layouts")
@section("layouts")


    @foreach($car as $cars)
        <div class="car1">
            @if(empty($cars->image[0]))
                <span class="img_profile_car"><img src="{{URL::to('/img/avatar_car.png')}}" width="30px" height="30px"></span>
            @else
                @if(count($cars->image)>0)
                    <span class="img_profile_car">
                        <div class="remove_image" data-id="{{$cars->image[0]->id}}">X</div>
                        <img src="{{URL::to('/storage/'.$cars->image[0]->image)}}" width="30px" height="30px">
                    </span>
                @endif
                <div class="car_image">
                    @foreach($cars->image as $image)
                        <br>
                        <div>
                            <div class="remove_image" data-id="{{$image->id}}">X</div>
                            <img src="{{URL::to('/storage/'.$image->image)}}" width="30px" height="30px">
                        </div>
                    @endforeach
                </div>
            @endif
            <div class="show_car">
                <span>Марка ` <span>{{$cars->model->brand->brand}}</span></span>
                <span>Модель ` <span>{{$cars->model->model}}</span></span>
                <span>Цвет ` <span class="color_name_edit">{{$cars->color}}</span></span>
                <span>Дата ` <span class="date_name_edit">{{$cars->date}}</span></span>
                <span>Пробег ` <span class="mileage_name_edit">{{$cars->mileage}}</span></span>
                <span>Цена ` <span class="price_name_edit">{{$cars->price}}</span></span>
            </div>
        </div>
        <form class="form_update">
            <h2>Update</h2>
            <div class="errors_ajax">
                <div style="display: none" class="errors_update_color_empty">color field is empty</div>
                <div style="display: none" class="errors_update_color_30">should not exceed 20 characters</div>
            </div>
            <input type="text" class="color" placeholder="color" value="{{$cars->color}}">
            <div class="errors_ajax">
                <div style="display: none" class="errors_update_date_empty">date field is empty</div>
                <div style="display: none" class="errors_update_date_number">must be in the date field</div>
                <div style="display: none" class="errors_update_date_10">should not exceed 10 characters</div>
            </div>
            <input type="number" class="date" placeholder="date" value="{{$cars->date}}">
            <div class="errors_ajax">
                <div style="display: none" class="errors_update_mileage_empty">mileage field is empty</div>
                <div style="display: none" class="errors_update_mileage_number">must be in the mileage field</div>
                <div style="display: none" class="errors_update_mileage_10">should not exceed 10 characters</div>
            </div>
            <input type="number" class="mileage" placeholder="mileage" value="{{$cars->mileage}}">
            <div class="errors_ajax">
                <div style="display: none" class="errors_update_price_empty">price field is empty</div>
                <div style="display: none" class="errors_update_price_number">must be in the price field</div>
                <div style="display: none" class="errors_update_price_10">should not exceed 10 characters</div>
            </div>
            <input type="number" class="price" placeholder="price" value="{{$cars->price}}">

            @csrf
            <input type="submit" class="send_update" data-id="{{$cars->id}}">
        </form>
    @endforeach
@endsection
