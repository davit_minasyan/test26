@extends("layouts/layouts")
@section("layouts")
    <div class="all">
            @foreach($cars as $car)
                <div class="car">
                    <a href="{{URL::to('Car/'.$car->id.'/edit')}}">
                        @if(empty($car->image[0]))
                            <span class="img_profile_car"><img src="{{URL::to('/img/avatar_car.png')}}" width="30px" height="30px"></span>
                        @else
                            @if(count($car->image)>0)
                                <span class="img_profile_car"><img src="{{URL::to('/storage/'.$car->image[0]->image)}}" width="30px" height="30px"></span>
                            @endif
                        @endif
                        <span>Марка {{$car->model->brand->brand}}` </span>
                        <span>Модель `{{$car->model->model}}</span>
                        <span>Цвет ` {{$car->color}}</span>
                        <span>Дата ` {{$car->date}}</span>
                        <span>Пробег ` {{$car->mileage}}</span>
                        <span>Цена ` {{$car->price}}</span>
                    </a>
                </div>
            @endforeach
    </div>
@endsection