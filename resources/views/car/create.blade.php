@extends("layouts/layouts")
@section("layouts")
    <form class="car_ajax"  enctype="multipart/form-data">
        <div style="display: none" class="errors_update_brand_empty">Brand field is empty</div>
        <select class="select">
            <option>Brand</option>
        @foreach($brand as $brand)
                <option class="option" data-id="{{$brand->id}}" value="{{$brand->id}}">{{$brand->brand}}</option>
            @endforeach
        </select>
        <div style="display: none" class="errors_update_model_empty">Model field is empty</div>
        <select class="select_model">
            <option>Model</option>
        </select>
        <div style="display: none" class="errors_update_color_empty">color field is empty</div>
        <div style="display: none" class="errors_update_color_30">should not exceed 20 characters</div>
        <input type="text" name="color" placeholder="Цвет" class="color">

        <div style="display: none" class="errors_update_date_empty">date field is empty</div>
        <div style="display: none" class="errors_update_date_number">must be in the date field</div>
        <div style="display: none" class="errors_update_date_10">should not exceed 10 characters</div>
        <input type="number" name="date" placeholder="Дата" class="date">

        <div style="display: none" class="errors_update_mileage_empty">mileage field is empty</div>
        <div style="display: none" class="errors_update_mileage_number">must be in the mileage field</div>
        <div style="display: none" class="errors_update_mileage_10">should not exceed 10 characters</div>
        <input type="number" name="mileage" placeholder="Пробег" class="mileage">

        <div style="display: none" class="errors_update_price_empty">price field is empty</div>
        <div style="display: none" class="errors_update_price_number">must be in the price field</div>
        <div style="display: none" class="errors_update_price_10">should not exceed 10 characters</div>
        <input type="number" name="price" placeholder="Цена" class="price">
        <input type="file" multiple="" name="image[]" class="image">
        <input type="submit" class="button_create_ajax">
    </form>
@endsection