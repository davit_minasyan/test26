@extends("layouts/layouts")
@section("layouts")
    <form method="post" class="form_login" action="{{URL::to('Confirm/enter')}}">
        <h1>Авторизоваться</h1>
        @if($errors->has("email"))
            <span>{{$errors->first("email")}}</span>
        @endif
        <input type="email" name="email" placeholder="электронное письмо">
        @if($errors->has("password"))
            <span>{{$errors->first("password")}}</span>
        @endif
        <input type="password" name="password" placeholder="пароль">
        @csrf
        <button>Отправить</button>
    </form>
@endsection
